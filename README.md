# L2INFO-ProjetS3


## Table des matières
1. [Description](#description)
2. [Configuration de la machine](#configuration-de-la-machine)
3. [Outils Utilisés pour le projet](#outils-utilisés-pour-le-projet)
    1. [Outils de développement](#outils-de-développement)
    2. [Outils de gestion de version](#outils-de-gestion-de-version)
    3. [Structure globale du programme](#structure-globale-du-programme)
    4. [Procédure de test](#procédure-de-test)
4. [Informations globales](#informations-globales)

## Description 

Résoudre le problème du voyageur de commerce. <br>
Langage : **C**

## Configuration de la machine

 Système d'exploitation : ParrotOS 4.7 (Linux)

## Outils Utilisés pour le projet

#### Outils de développement

- Outils de traitement de texte ou IDE : Gedit version : 3.30
- Compilation : GCC version 3

#### Outils de gestion de version
- Git : GitLab

#### Structure globale du programme
<p>Le programme sera structuré de la manière où chaque options (Brute force, algorithme génétique, etc.) sera associé a un fichier au format « .c » avec son entête au format « .h » de manière à inclure celui-ci dans un fichier « main.c » qui détiendra la fonction principale du programme.</p>

#### Procédure de test
<p>Le test du programme va s'effectuer de manière incrémentale. C'est-à-dire que chaque fonction sera testée au cas par cas et chaque test vérifiera que la fonction implémenté ne nuit pas aux fonctions ayant déjà passés les tests.</p>

Informations globales
============================================== 
Projet de S3 <br>
Par Kieffer Ewen
Licence 2 Informatique  | Groupe 3<br>
Université Toulouse III - Paul Sabatier<br>